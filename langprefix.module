<?php

/**
 * Adjust the language form at: admin/config/regional/language so that the path prefixes for each language
 * can be seen without viewing/editing the individual language.
 */

/**
 * Implements hook_theme().
 */
function langprefix_theme() {
  return array(
    'langprefix_locale_languages_overview_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Implements hook_form_FORMID_alter
 */
function langprefix_form_locale_languages_overview_form_alter(&$form, &$form_state) {
  $form['#theme'] = 'langprefix_locale_languages_overview_form';
}

/**
 * Custom override of theme_locale_languages_overview_form
 * We want to see the path prefix in the form as well, which is more significant for our purposes.
 * ----
 *  * Returns HTML for the language overview form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_langprefix_locale_languages_overview_form($variables) {
  //<hack>We need a language list available to get prefix
  $languages = language_list('language');
  //</hack>
  $form = $variables['form'];
  $default = language_default();
  foreach ($form['name'] as $key => $element) {
    // Do not take form control structures.
    if (is_array($element) && element_child($key)) {
      // Disable checkbox for the default language, because it cannot be disabled.
      if ($key == $default->language) {
        $form['enabled'][$key]['#attributes']['disabled'] = 'disabled';
      }

      // Add invisible labels for the checkboxes and radio buttons in the table
      // for accessibility. These changes are only required and valid when the
      // form is themed as a table, so it would be wrong to perform them in the
      // form constructor.
      $title = drupal_render($form['name'][$key]);
      $form['enabled'][$key]['#title'] = t('Enable !title', array('!title' => $title));
      $form['enabled'][$key]['#title_display'] = 'invisible';
      $form['site_default'][$key]['#title'] = t('Set !title as default', array('!title' => $title));
      $form['site_default'][$key]['#title_display'] = 'invisible';
      $rows[] = array(
        'data' => array(
          '<strong>' . $title . '</strong>',
          drupal_render($form['native'][$key]),
          check_plain($key),
          //<hacks> Add path prefix
          $languages[$key]->prefix,
          //</hacks>
          drupal_render($form['direction'][$key]),
          array('data' => drupal_render($form['enabled'][$key]), 'align' => 'center'),
          drupal_render($form['site_default'][$key]),
          drupal_render($form['weight'][$key]),
          l(t('edit'), 'admin/config/regional/language/edit/' . $key) . (($key != 'en' && $key != $default->language) ? ' ' . l(t('delete'), 'admin/config/regional/language/delete/' . $key) : '')
        ),
        'class' => array('draggable'),
      );
    }
  }
  $header = array(array('data' => t('English name')), array('data' => t('Native name')), array('data' => t('Code')), /*<hacks>*/array('data' => t('Path Prefix')),/*</hacks>*/array('data' => t('Direction')), array('data' => t('Enabled')), array('data' => t('Default')), array('data' => t('Weight')), array('data' => t('Operations')));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'language-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('language-order', 'order', 'sibling', 'language-order-weight');

  return $output;
}